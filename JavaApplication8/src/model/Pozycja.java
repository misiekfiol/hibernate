/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "POZYCJA")
public class Pozycja implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_POZYCJA")
    private int id_pozycja;
    @Column(name = "NAZWA")
    private String nazwa;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_POZYCJA")
    @ForeignKey(name = "FK_POZ_ZAW")
    private Set<Zawodnik> zawodnik = new HashSet<Zawodnik>(15);

    public Pozycja() {
    }

    public Pozycja(String nazwa) {
        this.nazwa = nazwa;
    }

    public Set<Zawodnik> getZawodnik() {
        return zawodnik;
    }

    public void setZawodnik(Set<Zawodnik> zawodnik) {
        this.zawodnik = zawodnik;
    }

    public void setId_pozycja(int id_pozycja) {
        this.id_pozycja = id_pozycja;
    }

    public int getId_pozycja() {
        return id_pozycja;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
