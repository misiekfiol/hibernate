/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "STADION")
public class Stadion implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_STADION")
    private int id_stadion;
    @Column(name = "NAZWA")
    private String nazwa;
    @Column(name = "LOKALIZACJA")
    private String lokalizacja;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_STADION")
    @ForeignKey(name = "FK_STAD_MECZ")
    private Mecz mecz;

    public Stadion() {

    }

    public Mecz getMecz() {
        return mecz;
    }

    public void setMecz(Mecz mecz) {
        this.mecz = mecz;
    }

    public void setId_stadion(int id_stadion) {
        this.id_stadion = id_stadion;
    }

    public int getId_stadion() {
        return id_stadion;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getLokalizacja() {
        return lokalizacja;
    }

    public void setLokalizacja(String lokalizacja) {
        this.lokalizacja = lokalizacja;
    }

    @Override
    public String toString() {
        return ("Nazwa: " + getNazwa() + " Lokalizacja: " + getLokalizacja() + " Mecz: " + getMecz());
    }
}
