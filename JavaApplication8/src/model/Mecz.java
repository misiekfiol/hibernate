/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "MECZ")
public class Mecz implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_MECZ")
    private int id_mecz;
    @Column(name = "TERMIN")
    private Calendar termin;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_MECZ")
    @ForeignKey(name = "FK_MECZ_DRUZ")
    private Set<Druzyna> druzyna = new HashSet<Druzyna>(2);
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_MECZ")
    @ForeignKey(name = "FK_MECZ_ZAW")
    private Set<Zawodnik> zawodnik = new HashSet<Zawodnik>(40);
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_MECZ")
    @ForeignKey(name = "FK_MECZ_STAT")
    private Set<Statystyki> statystyki = new HashSet<Statystyki>(20);
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "STADION", foreignKey = @javax.persistence.ForeignKey(name = "FK_MECZ_STAD"))
    private Stadion stadion;

    public Mecz() {

    }

    public Stadion getStadion() {
        return stadion;
    }

    public void setStadion(Stadion stadion) {
        this.stadion = stadion;
    }

    public Set<Statystyki> getStatystyki() {
        return statystyki;
    }

    public void setStatystyki(Set<Statystyki> statystyki) {
        this.statystyki = statystyki;
    }

    public Set<Druzyna> getDruzyna() {
        return druzyna;
    }

    public void setDruzyna(Set<Druzyna> druzyna) {
        this.druzyna = druzyna;
    }

    public Set<Zawodnik> getZawodnik() {
        return zawodnik;
    }

    public void setZawodnik(Set<Zawodnik> zawodnik) {
        this.zawodnik = zawodnik;
    }

    public void setId_mecz(int id_mecz) {
        this.id_mecz = id_mecz;
    }

    public int getId_mecz() {
        return id_mecz;
    }

    public Calendar getTermin() {
        return termin;
    }

    public void setTermin(Calendar termin) {
        this.termin = termin;
    }

}
