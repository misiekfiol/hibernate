/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author Michał Tworuszka
 */
// <joined-subclass extends="model.Osoba" lazy="false" name="model.Trener" table="TRENER">
// <joined-subclass extends="model.Osoba" lazy="false" name="model.Zawodnik" table="ZAWODNIK">

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="OSOBY")
public class Osoba implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID_OSOBA")
    private int id_osoba;
    @Column(name = "IMIE")
    private String imie;
    @Column(name = "NAZWISKO")
    private String nazwisko;

    public Osoba() {
    }

    public Osoba(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public int getId_osoba() {
        return id_osoba;
    }

    public void setId_osoba(int id_osoba) {
        this.id_osoba = id_osoba;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return ("Imie: " + getImie() + " Nazwisko: " + getNazwisko());
    }
}
