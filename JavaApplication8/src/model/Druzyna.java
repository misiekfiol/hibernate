/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "DRUZYNA")
public class Druzyna implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_DRUZYNA")
    private int id_Druzyna;
    @Column(name = "KRAJ")
    private String kraj;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_DRUZYNA")
    @ForeignKey(name = "FK_DRUZ_ZAW")
    private Set<Zawodnik> zawodnik = new HashSet<Zawodnik>(21);
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_DRUZYNA")
    @ForeignKey(name = "FK_DRUZ_TREN")
    private Set<Trener> trener = new HashSet<Trener>(3);
    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "DRUZYNA", foreignKey = @javax.persistence.ForeignKey(name = "FK_DRUZ_MECZ"))
    private Mecz mecz;

    public Druzyna() {

    }

    public Druzyna(String kraj) {
        this.kraj = kraj;
    }

    public Mecz getMecz() {
        return mecz;
    }

    public void setMecz(Mecz mecz) {
        this.mecz = mecz;
    }

    public Set<Trener> getTrener() {
        return trener;
    }

    public void setTrener(Set<Trener> trener) {
        this.trener = trener;
    }

    public Set<Zawodnik> getZawodnik() {
        return zawodnik;
    }

    public void setZawodnik(Set<Zawodnik> zawodnik) {
        this.zawodnik = zawodnik;
    }

    public void setId_Druzyna(int id_Druzyna) {
        this.id_Druzyna = id_Druzyna;
    }

    public int getId_Druzyna() {
        return id_Druzyna;
    }

    public String getKraj() {
        return kraj;
    }

    public void setKraj(String kraj) {
        this.kraj = kraj;
    }

    @Override
    public String toString() {
        return ("Kraj: " + getKraj() + " Mecz: " + getMecz() + " Trenerzy: " + getTrener() + " Zawodnicy: " + getZawodnik());
    }
}
