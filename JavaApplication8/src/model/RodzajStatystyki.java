/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "RODZAJ_STATYSTYKI")
public class RodzajStatystyki implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_RODZAJ_STATYSTYKI")
    private int id_RodzajStatystyki;
    @Column(name = "OPIS")
    private String opis;
    @ManyToMany(mappedBy = "statystyki")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<Statystyki> statystyki = new HashSet<Statystyki>(10);

    public RodzajStatystyki() {
    }

    public Set<Statystyki> getStatystyki() {
        return statystyki;
    }

    public void setStatystyki(Set<Statystyki> statystyki) {
        this.statystyki = statystyki;
    }
    public void setId_RodzajStatystyki(int id_RodzajStatystyki) {
        this.id_RodzajStatystyki = id_RodzajStatystyki;
    }

    public int getId_RodzajStatystyki() {
        return id_RodzajStatystyki;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
