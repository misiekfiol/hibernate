/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@PrimaryKeyJoinColumn(name = "ID_TRENER")
@ForeignKey(name = "FK_TREN_OS")
@Table(name = "TRENER")
public class Trener extends Osoba implements Serializable {

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "DRUZYNA", foreignKey = @javax.persistence.ForeignKey(name = "FK_TREN_DRUZ"))
    private Druzyna druzyna;

    public Trener() {
    }

    public Trener(String imie, String nazwisko) {
        super(imie, nazwisko);
    }

    public Druzyna getDruzyna() {
        return druzyna;
    }

    public void setDruzyna(Druzyna druzyna) {
        this.druzyna = druzyna;
    }

    @Override
    public String toString() {
        return ("Imie: " + getImie() + " Nazwisko: " + getNazwisko() + " Druzyna: " + getDruzyna());
    }

}
