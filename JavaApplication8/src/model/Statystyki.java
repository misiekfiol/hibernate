/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Michał Tworuszka
 */
@Entity
@Table(name = "STATYSTYKI")
public class Statystyki implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID_STATYSTYKI")
    private int id_statystyki;
    @Column(name = "WARTOSC")
    private int wartosc;
    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ID_STATYSTYKI")
    @ForeignKey(name = "FK_STAT_MECZ")
    private Set<Mecz> mecz = new HashSet<Mecz>(10);
    @ManyToMany
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinTable(name = "STAT_RODZ", joinColumns =@JoinColumn(name ="ID_STATYSTYKI"), inverseJoinColumns = @JoinColumn(name = "ID_RODZAJ_STATYSTYKI"))
    @ForeignKey(name = "FK_STAT_RODZ")
    private Set<RodzajStatystyki> rodzajStatystyki = new HashSet<RodzajStatystyki>(10);

    public Statystyki() {

    }

    public Set<RodzajStatystyki> getRodzajStatystyki() {
        return rodzajStatystyki;
    }

    public void setRodzajStatystyki(Set<RodzajStatystyki> rodzajStatystyki) {
        this.rodzajStatystyki = rodzajStatystyki;
    }

    public Set<Mecz> getMecz() {
        return mecz;
    }

    public void setMecz(Set<Mecz> mecz) {
        this.mecz = mecz;
    }

    public void setId_statystyki(int id_statystyki) {
        this.id_statystyki = id_statystyki;
    }

    public int getId_statystyki() {
        return id_statystyki;
    }

    public int getWartosc() {
        return wartosc;
    }

    public void setWartosc(int wartosc) {
        this.wartosc = wartosc;
    }

    @Override
    public String toString() {
        return ("Wartosc statystyki: " + getWartosc() + " Mecz: " + getMecz() + " Rodzaj Statystyki: " + getRodzajStatystyki());
    }
}
