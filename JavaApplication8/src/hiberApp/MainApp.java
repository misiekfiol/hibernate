/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hiberApp;

import model.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.sql.JoinType;

/**
 *
 * @author Michał Tworuszka
 */
public final class MainApp {

    private static final SessionFactory SESSION_FACTORY = HiberUtil.getSessionFactory(HiberUtil.Mapping.ANN);

    public static void main(String[] args) {
        createDruzyny();
        Session session = SESSION_FACTORY.openSession();
        Transaction tx = session.beginTransaction();
        System.out.println("###################################################");
        System.out.println("##################Zad 1############################");
        System.out.println("################## HQL ##############################");
        Query query = session.createQuery("select new list (z.imie, z.nazwisko) from Zawodnik z where z.imie like 'L%'");
        List zaw = query.list();
        int counter = zaw.size();
        for (int i = 0; i < counter; ++i) {
            System.out.println("#" + zaw.get(i).toString());
        }
        System.out.println("##################### KRYT ###########################");

        //kryterialne
        ProjectionList prList = Projections.projectionList();
        prList.add(Projections.property("imie"));
        prList.add(Projections.property("nazwisko"));
        List lista = session.createCriteria(Zawodnik.class)
                .setProjection(prList)
                .add(Restrictions.like("imie", "L%")).list();
        Iterator iter = lista.iterator();
        while (iter.hasNext()) {
            Object[] zaw1 = (Object[]) iter.next();
            System.out.println(zaw1[0] + " " + zaw1[1]);
        }
        System.out.println("###################################################");
        System.out.println("");
        System.out.println("");
        System.out.println("###################################################");
        System.out.println("##################Zad 2############################");
        System.out.println("################# HQL ##############################");

        //HQL
        query = session.createQuery("select new list (z.nazwisko, z.imie, d.kraj) from Druzyna d inner join d.zawodnik z where size(z.druzyna) >=6");
        List zaw2 = query.list();
        int counter1 = zaw2.size();
        for (int i = 0; i < counter1; ++i) {
            System.out.println("#" + zaw2.get(i).toString());
        }
        System.out.println("##################### KRYT ###########################");
        //kryteria
        Criteria crit = session.createCriteria(Druzyna.class, "druz")
                .createAlias("zawodnik", "zawod", JoinType.INNER_JOIN)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("druz.kraj"), "druzyna")
                        .add(Projections.property("zawod.imie"), "zawodnik")
                        .add(Projections.property("zawod.nazwisko"), "zawodnik"))
                .add(Restrictions.sizeGe("druz.zawodnik", 6));
        List listaDruz = crit.list();
        Iterator iter2 = listaDruz.iterator();
        while (iter2.hasNext()) {
            Object[] druz = (Object[]) iter2.next();
            System.out.println(druz[2] + " " + druz[1] + " " + druz[0]);
        }
        System.out.println("###################################################");
        System.out.println("");
        System.out.println("");
        System.out.println("###################################################");
        System.out.println("##################Zad 3############################");
        System.out.println("################## HQL #############################");

        //HQL
        query = session.createQuery("select new list(o.nazwisko, count(*)) from Osoba o group by o.nazwisko");
        List lDruz = query.list();
        int counter2 = lDruz.size();
        for (int i = 0; i < counter2; ++i) {
            System.out.println("#" + lDruz.get(i).toString());
        }
        System.out.println("##################### KRYT ###########################");
        Criteria crit2 = session.createCriteria(Osoba.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.groupProperty("nazwisko"))
                        .add(Projections.rowCount()));
        List listaOsob = crit2.list();
        Iterator iter3 = listaOsob.iterator();
        while (iter3.hasNext()) {
            Object[] osob = (Object[]) iter3.next();
            System.out.println(osob[0] + " " + osob[1]);
        }
        tx.commit();
        session.close();
    }

    private static void createZawodnicy(String imie, String nazwisko, Pozycja pozycja, HashSet<Zawodnik> zawodnicy, Druzyna druzyna) {
        Zawodnik z = new Zawodnik(imie, nazwisko);
        z.setPozycja(pozycja);
        z.setDruzyna(druzyna);
        zawodnicy.add(z);
    }

    private static void createTrenerzy(String imie, String nazwisko, HashSet<Trener> trenerzy, Druzyna druzyna) {
        Trener t = new Trener(imie, nazwisko);
        t.setDruzyna(druzyna);
        trenerzy.add(t);
    }

    private static void createDruzyny() {
        Session session = SESSION_FACTORY.openSession();
        Transaction tx = session.beginTransaction();
        //tworzenie pozycji
        Pozycja p1 = new Pozycja("Bramkarz");
        session.save(p1);
        Pozycja p2 = new Pozycja("Obrona");
        session.save(p2);
        Pozycja p3 = new Pozycja("Atak");
        session.save(p3);
        Pozycja p4 = new Pozycja("Rezerwa");
        session.save(p4);

        //tworzenie drużyny 1
        Druzyna d1 = new Druzyna("USA");

        //dodawanie zawodników
        HashSet<Zawodnik> zawodnicy1 = new HashSet<Zawodnik>(7);
        createZawodnicy("John", "Doe", p1, zawodnicy1, d1);
        createZawodnicy("Adam", "Pike", p2, zawodnicy1, d1);
        createZawodnicy("Mark", "Blunt", p2, zawodnicy1, d1);
        createZawodnicy("Steve", "White", p3, zawodnicy1, d1);
        createZawodnicy("Leon", "Snape", p3, zawodnicy1, d1);
        createZawodnicy("Jo", "Snape", p4, zawodnicy1, d1);
        createZawodnicy("Liroy", "Something", p4, zawodnicy1, d1);

        d1.setZawodnik(zawodnicy1);

        //dodawanie trenerów
        HashSet<Trener> trenerzy1 = new HashSet<Trener>(3);
        createTrenerzy("Mark", "Man", trenerzy1, d1);
        createTrenerzy("Paul", "Man", trenerzy1, d1);
        d1.setTrener(trenerzy1);
        session.save(d1);

        //tworzenie drużyny 2
        Druzyna d2 = new Druzyna("Anglia");
        HashSet<Zawodnik> zawodnicy2 = new HashSet<Zawodnik>(5);
        createZawodnicy("Lee", "Pike", p1, zawodnicy2, d2);
        createZawodnicy("Martin", "Osborn", p2, zawodnicy2, d2);
        createZawodnicy("Daniel", "Yellow", p2, zawodnicy2, d2);
        createZawodnicy("Steve", "Doe", p3, zawodnicy2, d2);
        createZawodnicy("Alan", "Harper", p3, zawodnicy2, d2);
        d2.setZawodnik(zawodnicy2);
        //dodawanie trenerów
        HashSet<Trener> trenerzy2 = new HashSet<Trener>(3);
        createTrenerzy("Tim", "Burton", trenerzy2, d2);
        createTrenerzy("John", "Lennon", trenerzy2, d2);
        createTrenerzy("Michael", "Knok", trenerzy2, d2);
        d2.setTrener(trenerzy2);
        session.save(d2);

        //tworzenie drużyny 3
        Druzyna d3 = new Druzyna("Polska");

        //dodawanie zawodników
        HashSet<Zawodnik> zawodnicy3 = new HashSet<Zawodnik>(7);
        createZawodnicy("Jerzy", "Lania", p1, zawodnicy3, d3);
        createZawodnicy("Adam", "Rower", p2, zawodnicy3, d3);
        createZawodnicy("Marek", "Blant", p2, zawodnicy3, d3);
        createZawodnicy("Stefan", "Bialy", p3, zawodnicy3, d3);
        createZawodnicy("Leon", "Zlamany", p3, zawodnicy3, d3);
        createZawodnicy("Jas", "Zlamany", p4, zawodnicy3, d3);
        createZawodnicy("Ludwik", "Costam", p4, zawodnicy3, d3);
        d3.setZawodnik(zawodnicy3);

        //dodawanie trenerów
        HashSet<Trener> trenerzy3 = new HashSet<Trener>(1);
        createTrenerzy("Ela", "Czarna", trenerzy3, d3);
        d3.setTrener(trenerzy3);
        session.save(d3);
        tx.commit();
        session.close();
    }
}
